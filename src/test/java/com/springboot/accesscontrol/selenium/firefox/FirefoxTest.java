package com.springboot.accesscontrol.selenium.firefox;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.springboot.accesscontrol.selenium.conf.TestConfig;

import java.net.MalformedURLException;
import java.net.URL;
import static org.junit.Assert.assertEquals;

public class FirefoxTest extends TestConfig {
	WebDriver driver;
	String baseUrl, nodeUrl;
	
	@Before
	public void setUp() throws MalformedURLException {
		baseUrl = "http://localhost:8087/accesscontrol/api/all/users";
		DesiredCapabilities caps = FirefoxTest.super.browser("firefox", "Windows", "10");
		driver = new RemoteWebDriver(new URL(FirefoxTest.super.getURL()), caps);
	}
	
	@Test
	public void appTest() {
		driver.get(baseUrl);
		assertEquals("Mantenedor de usuarios", driver.getTitle());
	}

	@After
	public void end() {
		driver.quit();
	}
}
