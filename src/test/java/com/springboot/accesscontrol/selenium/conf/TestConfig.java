package com.springboot.accesscontrol.selenium.conf;

import org.openqa.selenium.remote.DesiredCapabilities;

public class TestConfig {
	private final String BROWSERSTACK_USERNAME = "martinretamales1";
	private final String BROWSERSTACK_ACCESS_KEY = "qAarvx4SxuzFMUvMmpbB";
	private final boolean BROWSERSTACK_LOCAL = true;
	private final String browserstackLocalIdentifier = System.getenv("BROWSERSTACK_LOCAL_IDENTIFIER");
	private final String URL = "https://" + BROWSERSTACK_USERNAME + ":" + BROWSERSTACK_ACCESS_KEY + "@hub.browserstack.com/wd/hub";
	
	public DesiredCapabilities browser(String browser, String os, String os_version) {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("browser", browser);
		capabilities.setCapability("os", os);
        capabilities.setCapability("os_version", os_version);
		capabilities.setCapability("browserstack.local", BROWSERSTACK_LOCAL);
    	capabilities.setCapability("browserstack.localIdentifier", browserstackLocalIdentifier);
		return capabilities;
	}

	public String getURL() {
		return URL;
	}
	
}
